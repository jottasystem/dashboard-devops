from flask import Blueprint, render_template

jenkins = Blueprint('jenkins', __name__, url_prefix="/jenkins")


@jenkins.route('')
def index():
    return render_template('jenkins.html')